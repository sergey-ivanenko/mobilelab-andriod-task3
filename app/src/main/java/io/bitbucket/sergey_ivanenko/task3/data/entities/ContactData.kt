package io.bitbucket.sergey_ivanenko.task3.data.entities

import android.os.Parcelable
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.parcelize.Parcelize

@Entity(tableName = "contact_table")
@Parcelize
data class ContactData(
    @PrimaryKey(autoGenerate = false)
    @ColumnInfo(name = "id")
    val contactId: String,
    @ColumnInfo(name = "phone_number")
    val phoneNumber: String,
    @ColumnInfo(name = "first_name")
    val firstName: String? = "",
    @ColumnInfo(name = "last_name")
    val lastName: String? = "",
    val email: String? = ""
) : Parcelable