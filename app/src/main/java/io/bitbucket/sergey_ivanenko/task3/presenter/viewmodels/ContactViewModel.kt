package io.bitbucket.sergey_ivanenko.task3.presenter.viewmodels

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import io.bitbucket.sergey_ivanenko.task3.data.ContactDatabase
import io.bitbucket.sergey_ivanenko.task3.data.ContactRepository
import io.bitbucket.sergey_ivanenko.task3.data.entities.ContactData
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class ContactViewModel(application: Application) : AndroidViewModel(application) {

    private val contactDao = ContactDatabase.getDatabase(application).contactDao()
    private val contactRepository: ContactRepository by lazy { ContactRepository(contactDao) }
    val readAllContacts: LiveData<List<ContactData>> by lazy { contactRepository.readAllContacts() }

    fun addContact(contact: ContactData) {
        viewModelScope.launch(Dispatchers.IO) {
            contactRepository.addContact(contact)
        }
    }

    fun getContactByPhoneNumber(phoneNumber: String): LiveData<ContactData> {
        return contactRepository.getContactByPhoneNumber(phoneNumber)
    }
}